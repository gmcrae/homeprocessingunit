class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.string :host
      t.string :manufacture
      t.string :serial_number
      t.string :secre
      t.integer :port

      t.timestamps
    end
  end
end
